defmodule MyMoviesApi.Repo.Migrations.CreateMovies do
  use Ecto.Migration

  def change do
    create table(:movies) do
      add :movie_id, :integer
      add :watched, :boolean, default: false, null: false
      add :liked, :boolean, null: true
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:movies, [:user_id, :movie_id], unique: true)

  end
end
