defmodule MyMoviesApi.Repo do
  use Ecto.Repo,
    otp_app: :my_movies_api,
    adapter: Ecto.Adapters.Postgres
end
