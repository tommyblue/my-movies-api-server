defmodule MyMoviesApi.AuthToken do
  use Ecto.Schema
  import Ecto.Changeset


  schema "auth_tokens" do
    belongs_to :user, MyMoviesApi.User

    field :revoked, :boolean, default: false
    field :revoked_at, :utc_datetime
    field :token, :string

    timestamps()
  end

  @doc false
  def changeset(%MyMoviesApi.AuthToken{} = auth_token, attrs) do
    auth_token
    |> cast(attrs, [:token])
    |> validate_required([:token])
    |> unique_constraint(:token)
  end
end
