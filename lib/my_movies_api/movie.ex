defmodule MyMoviesApi.Movie do
  use Ecto.Schema
  import Ecto.Changeset

  schema "movies" do
    field :liked, :boolean, default: nil
    field :movie_id, :integer # Maps id in themoviedb
    field :watched, :boolean, default: false
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(movie, attrs) do
    movie
    |> cast(attrs, [:user_id, :movie_id, :watched])
    |> validate_required([:user_id, :movie_id, :watched])
    |> unique_constraint(:movie_id, name: :movies_user_id_movie_id_index)
  end

  def update_changeset(movie, attrs) do
    movie
    |> cast(attrs, [:watched, :liked])
    |> validate_required([:watched])
  end
end
