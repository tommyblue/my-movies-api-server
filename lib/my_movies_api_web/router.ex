defmodule MyMoviesApiWeb.Router do
  use MyMoviesApiWeb, :router
  alias MyMoviesApiWeb.Api

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticate do
    plug MyMoviesApiWeb.Plugs.Authenticate
  end

  scope "/api", Api do
    pipe_through :api

    scope "/session" do
      post "/sign_in", SessionController, :signin
      post "/sign_up", SessionController, :signup
      delete "/sign_out", SessionController, :signout
    end

    scope "/v1", V1 do
      pipe_through :authenticate

      resources "/movies", MoviesController, only: [:index, :create, :update, :delete]
    end
  end
end
