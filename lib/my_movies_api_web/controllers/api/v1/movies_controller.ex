defmodule MyMoviesApiWeb.Api.V1.MoviesController do
  use MyMoviesApiWeb, :controller
  import Ecto.Query, only: [from: 2]

  alias MyMoviesApi.Movie
  alias MyMoviesApi.Repo
  alias MyMoviesApiWeb.Api.V1.ChangesetView

  def index(conn, _params) do
    movies = get_movies(conn.assigns.signed_user.id)
    render conn, "index.json", movies: movies
  end

  def create(conn, movie_params) do
    movie_params = Map.merge(movie_params, %{"user_id" => conn.assigns.signed_user.id})
    changeset = Movie.changeset(%Movie{}, movie_params)
    case Repo.insert(changeset) do
      {:ok, movie} ->
        conn
        |> put_status(:created)
        |> render("show.json", movie: movie)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => movie_id, "watched" => watched, "liked" => liked}) do
    movie = get_movie(conn.assigns.signed_user.id, movie_id)
    changeset = Movie.update_changeset(movie, %{
      "watched" => watched,
      "liked" => liked
    })

    case Repo.update(changeset) do
      {:ok, movie} ->
        conn
        |> put_status(:ok)
        |> render("show.json", movie: movie)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => movie_id}) do
    movie = get_movie(conn.assigns.signed_user.id, movie_id)
    Repo.delete!(movie)
    conn
    |> send_resp(204, "")
  end

  defp get_movies(user_id) do
    from(m in Movie, where: m.user_id == ^user_id)
    |> Repo.all
  end

  defp get_movie(user_id, movie_id) do
    from(m in Movie, where: m.user_id == ^user_id and m.id == ^movie_id)
    |> Repo.one!
  end
end
