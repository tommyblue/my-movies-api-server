defmodule MyMoviesApiWeb.Api.SessionController do
  use MyMoviesApiWeb, :controller
  alias MyMoviesApi.User
  alias MyMoviesApi.Repo
  alias MyMoviesApiWeb.Api.V1.ChangesetView
  alias MyMoviesApiWeb.ErrorView

  def signup(conn, %{"email" => email, "password" => password}) do
    changeset = User.changeset(%User{}, %{
      email: email,
      password: password
    })
    case Repo.insert(changeset) do
      {:ok, _} ->
        case User.sign_in(email, password) do
          {:ok, auth_token} ->
            conn
            |> put_status(201)
            |> render("show.json", auth_token)
          {:error, reason} ->
            conn
            |> send_resp(401, reason)
        end
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(ChangesetView, "error.json", changeset: changeset)
    end
  end

  def signin(conn, %{"email" => email, "password" => password}) do
    case User.sign_in(email, password) do
      {:ok, auth_token} ->
        conn
        |> put_status(:ok)
        |> render("show.json", auth_token)
      {:error, reason} ->
        conn
        |> put_status(401)
        |> render(ErrorView, "error.json", reason: reason)
    end
  end

  def signout(conn, _) do
    case User.sign_out(conn) do
      {:error, reason} -> conn |> send_resp(400, reason)
      {:ok, _} -> conn |> send_resp(204, "")
    end
  end
end
