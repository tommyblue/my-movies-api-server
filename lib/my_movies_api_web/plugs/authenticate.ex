defmodule MyMoviesApiWeb.Plugs.Authenticate do
  import Plug.Conn
  alias MyMoviesApiWeb.ErrorView

  def init(default), do: default

  def call(conn, _default) do
    case MyMoviesApi.Services.Authenticator.get_auth_token(conn) do
      {:ok, token} ->
        case MyMoviesApi.Repo.get_by(MyMoviesApi.AuthToken, %{token: token, revoked: false})
        |> MyMoviesApi.Repo.preload(:user) do
          nil -> unauthorized(conn)
          auth_token -> authorized(conn, auth_token.user)
        end
      _ -> unauthorized(conn)
    end
  end

  defp authorized(conn, user) do
    conn
    |> assign(:signed_in, true)
    |> assign(:signed_user, user)
  end

  defp unauthorized(conn) do
    conn
    |> put_status(401)
    |> Phoenix.Controller.render(ErrorView, "error.json", reason: "Unauthorized")
    |> halt()
  end
end
