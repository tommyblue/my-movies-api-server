defmodule MyMoviesApiWeb.Api.SessionView do
  use MyMoviesApiWeb, :view

  def render("show.json", auth_token) do
    %{data: %{token: auth_token.token}}
  end
end
