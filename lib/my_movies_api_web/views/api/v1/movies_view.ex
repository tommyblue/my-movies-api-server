defmodule MyMoviesApiWeb.Api.V1.MoviesView do
  use MyMoviesApiWeb, :view

  def render("index.json", %{movies: movies}) do
    %{data: render_many(movies, __MODULE__, "movie.json", as: :movie)}
  end

  def render("show.json", %{movie: movie}) do
    %{data: render_one(movie, __MODULE__, "movie.json", as: :movie)}
  end

  def render("movie.json", %{movie: movie}) do
    %{
      id: movie.id,
      movie_id: movie.movie_id,
      watched: movie.watched,
      liked: movie.liked
    }
  end
end
