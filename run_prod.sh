#!/bin/sh

export MIX_ENV=prod
export MIX_HOME=/root/.mix

mix ecto.create
mix ecto.migrate
mix phx.server
