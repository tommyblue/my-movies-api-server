# My Movies - Documentazione degli endpoint

## Overview

Questa documentazione è stata generata con [swagger-codegen](https://github.com/swagger-api/swagger-codegen).

### Lanciare il server

Installare i moduli richiesti con:

```
npm install
```

Dopodichè lanciare il server con:

```
npm start
```

L'interfaccia di Swagger sarà disponibile all'indirizzo http://localhost:4001/docs
