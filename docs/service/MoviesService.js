'use strict';


/**
 * Get all the movies for the user
 * 
 *
 * returns MoviesResponse
 **/
exports.moviesGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "watched" : true,
    "id" : 0,
    "movie_id" : 6,
    "liked" : true
  }, {
    "watched" : true,
    "id" : 0,
    "movie_id" : 6,
    "liked" : true
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Remove a movie from the list
 * 
 *
 * movieId Long ID of the movie
 * no response value expected for this operation
 **/
exports.moviesMovieIdDELETE = function(movieId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Add a new movie to the user list
 * 
 *
 * movieId Long ID of the movie
 * body NewMovieRequest Movie to be added to the user list
 * returns MovieResponse
 **/
exports.moviesMovieIdPOST = function(movieId,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "watched" : true,
    "id" : 0,
    "movie_id" : 6,
    "liked" : true
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change a property of the movie
 * 
 *
 * movieId Long ID of the movie
 * body UpdateMovieRequest Movie to be updated
 * returns MovieResponse
 **/
exports.moviesMovieIdPUT = function(movieId,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "watched" : true,
    "id" : 0,
    "movie_id" : 6,
    "liked" : true
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

