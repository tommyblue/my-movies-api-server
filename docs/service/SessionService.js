'use strict';


/**
 * User login
 * 
 *
 * body UserRequest User informations, required to sign in
 * returns UserToken
 **/
exports.sessionSign_inPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "token" : "token"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * User logout
 * 
 *
 * no response value expected for this operation
 **/
exports.sessionSign_outDELETE = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * New user registration
 * 
 *
 * body UserRequest User informations, required to sign up
 * returns UserToken
 **/
exports.sessionSign_upPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "token" : "token"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

