'use strict';

var utils = require('../utils/writer.js');
var Movies = require('../service/MoviesService');

module.exports.moviesGET = function moviesGET (req, res, next) {
  Movies.moviesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.moviesMovieIdDELETE = function moviesMovieIdDELETE (req, res, next) {
  var movieId = req.swagger.params['movieId'].value;
  Movies.moviesMovieIdDELETE(movieId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.moviesMovieIdPOST = function moviesMovieIdPOST (req, res, next) {
  var movieId = req.swagger.params['movieId'].value;
  var body = req.swagger.params['body'].value;
  Movies.moviesMovieIdPOST(movieId,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.moviesMovieIdPUT = function moviesMovieIdPUT (req, res, next) {
  var movieId = req.swagger.params['movieId'].value;
  var body = req.swagger.params['body'].value;
  Movies.moviesMovieIdPUT(movieId,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
