'use strict';

var utils = require('../utils/writer.js');
var Session = require('../service/SessionService');

module.exports.sessionSign_inPOST = function sessionSign_inPOST (req, res, next) {
  var body = req.swagger.params['body'].value;
  Session.sessionSign_inPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sessionSign_outDELETE = function sessionSign_outDELETE (req, res, next) {
  Session.sessionSign_outDELETE()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sessionSign_upPOST = function sessionSign_upPOST (req, res, next) {
  var body = req.swagger.params['body'].value;
  Session.sessionSign_upPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
