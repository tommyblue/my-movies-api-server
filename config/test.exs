use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :my_movies_api, MyMoviesApiWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :my_movies_api, MyMoviesApi.Repo,
  username: "postgres",
  password: "postgres",
  database: "my_movies_api_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
