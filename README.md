# My Movies API server

## Requisiti

[Docker](https://www.docker.com/) e [docker-compose](https://docs.docker.com/compose/)

## Prima esecuzione

Al primo build Docker dovrà scaricare le immagini e fare il setup. Richiede qualche minuto e qualche GB di download.

```
docker-compose build
```

## Lanciare il server

```
docker-compose up
```

Docker lancerà un container PostgreSQL e un container con il server API realizzato con [Phoenix](https://phoenixframework.org/)

Una volta pronto, il server sarà in ascolto all'indirizzo http://localhost:4000/

## Documentazione degli endpoint

Nella cartella [docs](./docs) trovate la documentazione realizzata con [Swagger](https://swagger.io/)

## Deploy su Heroku

È possibile effettuare il deploy su Heroku utilizzando le loro risorse gratuite.

Una volta registrati sul sito installare la CLI ed effettuare il login dalla console con `heroku login`

Creare quindi l'app e settarla come container (per poter eseguire Docker):

```
heroku create
heroku stack:set container
heroku container:login
```

Aggiungere l'addon con PostgreSQL e configurare le variabili d'ambiente.
Il secret per `SECRET_KEY_BASE` può essere generato con `mix phx.gen.secret`

```
heroku addons:create heroku-postgresql:hobby-dev
heroku config:set SECRET_KEY_BASE="<secret>"
heroku config:set POOL_SIZE=18
heroku config:set MIX_ENV="prod"
```

Effettuare quindi la build dell'immagine e il deploy:

```
cd deploy
./release_heroku.sh
```

Per lanciare l'app sul browser eseguire `heroku open`
