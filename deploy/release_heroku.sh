#!/bin/sh

set -e

heroku container:push web \
    --arg SECRET_KEY_BASE="$(heroku config:get SECRET_KEY_BASE)" \
    --context-path ../

heroku container:release web
